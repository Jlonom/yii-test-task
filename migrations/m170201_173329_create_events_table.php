<?php

use yii\db\Migration;

/**
 * Handles the creation of table `events`.
 * Has foreign keys to the tables:
 *
 * - `areas`
 * - `show`
 */
class m170201_173329_create_events_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('events', [
            'id' => $this->primaryKey(),
            'date' => $this->date()->defaultValue(0000-00-00),
            'area_id' => $this->integer(),
            'show_id' => $this->integer(),
        ]);

        // creates index for column `area_id`
        $this->createIndex(
            'idx-events-area_id',
            'events',
            'area_id'
        );

        // add foreign key for table `areas`
        $this->addForeignKey(
            'fk-events-area_id',
            'events',
            'area_id',
            'areas',
            'id',
            'CASCADE'
        );

        // creates index for column `show_id`
        $this->createIndex(
            'idx-events-show_id',
            'events',
            'show_id'
        );

        // add foreign key for table `show`
        $this->addForeignKey(
            'fk-events-show_id',
            'events',
            'show_id',
            'show',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `areas`
        $this->dropForeignKey(
            'fk-events-area_id',
            'events'
        );

        // drops index for column `area_id`
        $this->dropIndex(
            'idx-events-area_id',
            'events'
        );

        // drops foreign key for table `show`
        $this->dropForeignKey(
            'fk-events-show_id',
            'events'
        );

        // drops index for column `show_id`
        $this->dropIndex(
            'idx-events-show_id',
            'events'
        );

        $this->dropTable('events');
    }
}
