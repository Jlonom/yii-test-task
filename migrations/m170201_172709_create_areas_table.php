<?php

use yii\db\Migration;

/**
 * Handles the creation of table `areas`.
 */
class m170201_172709_create_areas_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('areas', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'photo' => $this->string(255)->notNull(),
            'description' => $this->text(),
            'sort' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('areas');
    }
}
