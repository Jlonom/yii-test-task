<?php

use yii\db\Migration;

/**
 * Handles the creation of table `show`.
 */
class m170201_172830_create_show_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('show', [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'image' => $this->string(255)->notNull(),
            'description' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('show');
    }
}
