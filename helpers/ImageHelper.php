<?php
/**
 * Created by PhpStorm.
 * User: jlonom
 * Date: 02.02.17
 * Time: 2:59
 */

namespace app\helpers;
use Yii;
use yii\base\ErrorException;


class ImageHelper
{
    public static function getThumbnail($src, $width, $height)
    {
        $img_name = array_pop(explode("/", $src));
        $tmp = explode(".", $img_name);
        $extension = array_pop($tmp);
        if(file_exists(Yii::getAlias(Yii::$app->modules['user']['userPhotoPath']) . "/" . implode(".", $tmp) . "-" . $width . "x" . $height . "." . $extension))
        {
            return Yii::$app->modules['user']['userPhotoUrl'] . "/" . implode(".", $tmp) . "-" . $width . "x" . $height . "." . $extension;
        }else
            throw new ErrorException(Yii::getAlias(Yii::$app->modules['user']['userPhotoPath']) . "/" . implode(".", $tmp) . "-" . $width . "x" . $height . "." . $extension . " not found!");
    }
}