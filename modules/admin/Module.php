<?php
/**
 * Created by PhpStorm.
 * User: jlonom
 * Date: 01.02.17
 * Time: 18:09
 */

namespace app\modules\admin;

class Module extends \yii\base\Module
{
    public $photoSizes;

    public function init()
    {
        $this->layout = "main";
        parent::init();
    }
}