<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Show */
?>
<div class="show-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'image',
            'description:ntext',
        ],
    ]) ?>

</div>
