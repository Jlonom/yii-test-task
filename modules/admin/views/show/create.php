<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Show */

?>
<div class="show-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
