<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Show */
?>
<div class="show-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
