<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Events */

?>
<div class="events-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
