<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Events */
?>
<div class="events-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date',
            'area_id',
            'show_id',
        ],
    ]) ?>

</div>
