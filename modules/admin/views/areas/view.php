<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Areas */
?>
<div class="areas-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'photo',
            'description:ntext',
            'sort',
        ],
    ]) ?>

</div>
