<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Areas */
?>
<div class="areas-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
