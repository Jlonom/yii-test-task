<?php
/**
 * Created by PhpStorm.
 * User: jlonom
 * Date: 01.02.17
 * Time: 19:38
 */

namespace app\modules\admin\controllers;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use budyaga\users\models\forms\LoginForm;


class DefaultController extends \yii\web\Controller
{

    public function actions()
    {
        return [
            'uploadPhoto' => [
                'class' => 'app\modules\admin\actions\UploadAction',
                'url' => Yii::$app->modules['user']['userPhotoUrl'],
                'path' => Yii::$app->modules['user']['userPhotoPath'],
            ]
        ];
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render("index");
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect("admin/default/index");
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }
}