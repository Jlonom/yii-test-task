<?php
/**
 * Created by PhpStorm.
 * User: jlonom
 * Date: 02.02.17
 * Time: 1:04
 */

namespace app\modules\admin\actions;

use yii\base\DynamicModel;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\Box;
use Yii;

class UploadAction extends \budyaga\cropper\actions\UploadAction
{

    public $sizes;
    /**
     * @inheritdoc
     */
    public function run()
    {
        if (Yii::$app->request->isPost) {

            $this->sizes = Yii::$app->modules['admin']->photoSizes;

            $file = UploadedFile::getInstanceByName($this->uploadParam);
            $model = new DynamicModel(compact($this->uploadParam));
            $model->addRule($this->uploadParam, 'image', [
                'maxSize' => $this->maxSize,
                'tooBig' => Yii::t('cropper', 'TOO_BIG_ERROR', ['size' => $this->maxSize / (1024 * 1024)]),
                'extensions' => explode(', ', $this->extensions),
                'wrongExtension' => Yii::t('cropper', 'EXTENSION_ERROR', ['formats' => $this->extensions])
            ])->validate();

            if ($model->hasErrors()) {
                $result = [
                    'error' => $model->getFirstError($this->uploadParam)
                ];
            } else {
                $request = Yii::$app->request;
                $uniqid = uniqid();
                $extension = $model->{$this->uploadParam}->extension;
                $saved = false;
                foreach($this->sizes as $size)
                {
                    $name = $uniqid . "-" . $size['width'] . 'x' . $size['height'] . '.' . $extension;

                    $image = Image::crop(
                        $file->tempName . $request->post('filename'),
                        intval($request->post('w')),
                        intval($request->post('h')),
                        [$request->post('x'), $request->post('y')]
                    )->resize(
                        new Box($size['width'], $size['height'])
                    );

                    if ($image->save($this->path . $name)){
                        $saved = true;
                    }
                }


                if ($saved === true) {

                    $width = $request->post('width', $this->width);
                    $height = $request->post('height', $this->height);

                    $image = Image::crop(
                        $file->tempName . $request->post('filename'),
                        intval($request->post('w')),
                        intval($request->post('h')),
                        [$request->post('x'), $request->post('y')]
                    )->resize(
                        new Box($width, $height)
                    );
                    $image->save($this->path . $uniqid . '.' . $extension);

                    $result = [
                        'filelink' => $this->url . $uniqid . '.' . $extension,
                    ];
                } else {
                    $result = [
                        'error' => Yii::t('cropper', 'ERROR_CAN_NOT_UPLOAD_FILE')]
                    ;
                }
            }
            Yii::$app->response->format = Response::FORMAT_JSON;

            return $result;
        } else {
            throw new BadRequestHttpException(Yii::t('cropper', 'ONLY_POST_REQUEST'));
        }
    }
}