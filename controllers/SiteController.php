<?php

namespace app\controllers;

use app\models\Areas;
use app\models\Events;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' =>  Events::find()->orderBy("date ASC")->where("date >= :date", [":date" => date("Y-m-d")]),
            'pagination' => [
                'pageSize' => 9,
            ],

        ]);
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }

    public function actionViewArea($id)
    {
        if (($model = Areas::findOne($id)) !== null) {
            $dataProvider = new ActiveDataProvider([
                'query' =>  Events::find()->orderBy("date ASC")->where("date >= :date", [":date" => date("Y-m-d")])->andWhere("area_id = :aid", [":aid" => $id]),
                'pagination' => [
                    'pageSize' => 6,
                ],
            ]);
            return $this->render("view-area", [
                "model" =>  $model,
                "dataProvider"  =>  $dataProvider,
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAreas()
    {
        $dataProvider = new ActiveDataProvider([
            "query" =>  Areas::find(),
            'pagination' => [
                'pageSize' => 3,
            ],
        ]);
        return $this->render("areas", ["dataProvider" => $dataProvider]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


}
