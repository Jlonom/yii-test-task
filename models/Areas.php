<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%areas}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $photo
 * @property string $description
 * @property integer $sort
 */
class Areas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%areas}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'photo'], 'required'],
            [['description'], 'string'],
            [['sort'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['photo'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'photo' => 'Photo',
            'description' => 'Description',
            'sort' => 'Sort',
        ];
    }
}
