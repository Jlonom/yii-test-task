<?php
/**
 * @var $model app\models\Areas
 */
use yii\helpers\Html;
use yii\helpers\Url;
use app\helpers\ImageHelper;
?>
<div class="col-sm-6 col-md-3 list-item">
    <div class="thumbnail">
        <?=Html::img(
            !empty($model->photo) ? ImageHelper::getThumbnail($model->photo, 800, 500) : "https://placeholdit.imgix.net/~text?txtsize=72&txt=800%C3%97500&w=800&h=500",
            ["class" => "img-responsive"])?>
        <div class="caption">
            <?=Html::tag("h3", $model->name)?>
            <?=Html::tag("p", mb_substr($model->description, 0, 128) . "...")?>
            <?=Html::a("Просмотр событий", Url::to(["/site/view-area", "id" => $model->id]), ["class" => "btn btn-primary"])?>
        </div>
    </div>
</div>