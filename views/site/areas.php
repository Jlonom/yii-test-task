<?php
use yii\widgets\ListView;
/* @var $this yii\web\View */

$this->title = 'Площадки';
?>
<div class="site-index">
    <?=
    ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item-area',
        'itemOptions'   =>  [
            'tag'   => false
        ],
        'options' => [
        ],
        'layout' => "<div class='row text-center'>{items}</div>\n<div class='row text-center'><div class='col-xs-12'>{pager}</div></div>",
        'pager' => [
            'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left"></span>',
            'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right"></span>',
            'maxButtonCount' => 3,
        ],
    ])
    ?>
</div>
