<?php
/**
 * @var $model app\models\Areas
 */
use yii\helpers\Html;
use app\helpers\ImageHelper;
use yii\widgets\ListView;
?>
<div class="area-content">
    <div class="row">
        <div class="col-md-8">
            <?=Html::img(
                !empty($model->photo) ? ImageHelper::getThumbnail($model->photo, 900, 350) : "https://placeholdit.imgix.net/~text?txtsize=72&txt=900%C3%97350&w=900&h=350",
                ["class" => "img-responsive"])?>
        </div>
        <div class="col-md-4">
            <?=Html::tag("h1", $model->name)?>
            <?=Html::tag("p", $model->description)?>
        </div>
    </div>
    <div class="row">
        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_item-event',
            'options' => [
                'tag' => 'div',
                'class' => 'row',
                'id' => 'events-list',
            ],
            'layout' => "{items}\n<div class='row text-center'><div class='col-xs-12'>{pager}</div></div>",
            'pager' => [
                'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left"></span>',
                'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right"></span>',
                'maxButtonCount' => 3,
            ],
        ])
        ?>
    </div>
</div>
