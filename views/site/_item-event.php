<?php
/* @var $model app\models\Events */
use yii\helpers\Html;
use yii\helpers\Url;
use app\helpers\ImageHelper;
?>
<div class="col-md-4 list-item">
    <?=Html::img(
        !empty($model->show->image) ? ImageHelper::getThumbnail($model->show->image, 700, 400) : "https://placeholdit.imgix.net/~text?txtsize=72&txt=700%C3%97400&w=700&h=400",
        ["class" => "img-responsive"])?>
    <?=Html::tag("h3", $model->show->name)?>
    <?=Html::tag("span", $model->date, ["class" => "date"])?>
    <?=Html::a(Html::tag("h4", $model->area->name), Url::to(["/site/view-area", "id" => $model->area->id]))?>
    <?=Html::tag("p", mb_substr($model->show->description, 0, 255) . "...")?>
</div>
