<?php
use yii\widgets\ListView;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <?=
    ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item-event',
        'options' => [
            'tag' => 'div',
            'class' => 'row',
            'id' => 'events-list',
        ],
        'layout' => "{items}\n<div class='row text-center'><div class='col-xs-12'>{pager}</div></div>",
        'pager' => [
            'prevPageLabel' => '<span class="glyphicon glyphicon-chevron-left"></span>',
            'nextPageLabel' => '<span class="glyphicon glyphicon-chevron-right"></span>',
            'maxButtonCount' => 3,
        ],
    ])
    ?>
</div>
